# Activiti-Modeler

## 介绍
+ 这是一个基于SpringBoot(1.5.2.RELEASE)+Activiti(5.22.0)的流程设计器 Activiti Modeler
+ 因为要学Activiti7，IDE插件的设计器版本问题不能使用，所以Git上找了一个开源的Activiti Modeler架子，做了一些功能的扩展。
+ 该设计器为汉化版

#### 原来的项目：
[参考博客：https://www.cnblogs.com/rmxd/p/11715016.html#_label2_0](https://www.cnblogs.com/rmxd/p/11715016.html#_label2_0)

[参考github地址 ：https://github.com/Simple-Coder/springboot-activiti](https://github.com/Simple-Coder/springboot-activiti)


## 软件架构
基于BS架构的流程设计器 Activiti Modeler，前后端不分离。技术栈：
+ 后端：
    - SpringBoot(1.5.2.RELEASE)
    - Activiti(5.22.0)
    - thymeleaf
    - mysql
+ 前端：
    + Vue
    + ElementUI
## 使用说明

设计器访问地址：http://localhost:8083/model-list.html

### 主页：展示模型数据列表
![主页](/imgs/mian.png)
### 新建模型
![主页](/imgs/new.png)

![主页](/imgs/moldEditor.png)
### XML预览
![主页](/imgs/xml.png)
### 流程图预览
![](/imgs/picture.png)
### 流程图XML文件导出
![](/imgs/out.png)
```xml
<?xml version="1.0" encoding="UTF-8"?>
<definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:activiti="http://activiti.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" typeLanguage="http://www.w3.org/2001/XMLSchema" expressionLanguage="http://www.w3.org/1999/XPath" targetNamespace="http://www.activiti.org/processdef">
  <process id="process" isExecutable="true">
    <startEvent id="sid-69513423-593F-4E27-B4C5-9E8EDAB17BF3"></startEvent>
    <userTask id="sid-ACB7181F-9B60-4F58-A541-95A350ED6610"></userTask>
    <sequenceFlow id="sid-E51B5FA9-FEEA-4FD5-AD16-653812CD5DB0" sourceRef="sid-69513423-593F-4E27-B4C5-9E8EDAB17BF3" targetRef="sid-ACB7181F-9B60-4F58-A541-95A350ED6610"></sequenceFlow>
    <userTask id="sid-CBFC543F-CF3D-4C6E-BBA6-6E26FA8CDACC"></userTask>
    <sequenceFlow id="sid-F6EE8655-2911-41B1-A275-1EC0670FF7D5" sourceRef="sid-ACB7181F-9B60-4F58-A541-95A350ED6610" targetRef="sid-CBFC543F-CF3D-4C6E-BBA6-6E26FA8CDACC"></sequenceFlow>
    <userTask id="sid-7D5C1AE6-97B3-4902-B5B8-1AA2E3EB0354"></userTask>
    <sequenceFlow id="sid-5DFF304D-4C44-46B6-8FAD-9C235B0C6A74" sourceRef="sid-CBFC543F-CF3D-4C6E-BBA6-6E26FA8CDACC" targetRef="sid-7D5C1AE6-97B3-4902-B5B8-1AA2E3EB0354"></sequenceFlow>
    <endEvent id="sid-AFFC9601-71B7-46CF-9A46-A630C02BF3EB"></endEvent>
    <sequenceFlow id="sid-90D7C165-4154-4329-AB3D-148B8575629C" sourceRef="sid-7D5C1AE6-97B3-4902-B5B8-1AA2E3EB0354" targetRef="sid-AFFC9601-71B7-46CF-9A46-A630C02BF3EB"></sequenceFlow>
  </process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_process">
    <bpmndi:BPMNPlane bpmnElement="process" id="BPMNPlane_process">
      <bpmndi:BPMNShape bpmnElement="sid-69513423-593F-4E27-B4C5-9E8EDAB17BF3" id="BPMNShape_sid-69513423-593F-4E27-B4C5-9E8EDAB17BF3">
        <omgdc:Bounds height="30.0" width="30.0" x="115.5" y="212.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="sid-ACB7181F-9B60-4F58-A541-95A350ED6610" id="BPMNShape_sid-ACB7181F-9B60-4F58-A541-95A350ED6610">
        <omgdc:Bounds height="80.0" width="100.0" x="190.5" y="187.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="sid-CBFC543F-CF3D-4C6E-BBA6-6E26FA8CDACC" id="BPMNShape_sid-CBFC543F-CF3D-4C6E-BBA6-6E26FA8CDACC">
        <omgdc:Bounds height="80.0" width="100.0" x="335.5" y="187.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="sid-7D5C1AE6-97B3-4902-B5B8-1AA2E3EB0354" id="BPMNShape_sid-7D5C1AE6-97B3-4902-B5B8-1AA2E3EB0354">
        <omgdc:Bounds height="80.0" width="100.0" x="480.5" y="187.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape bpmnElement="sid-AFFC9601-71B7-46CF-9A46-A630C02BF3EB" id="BPMNShape_sid-AFFC9601-71B7-46CF-9A46-A630C02BF3EB">
        <omgdc:Bounds height="28.0" width="28.0" x="625.5" y="213.0"></omgdc:Bounds>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge bpmnElement="sid-E51B5FA9-FEEA-4FD5-AD16-653812CD5DB0" id="BPMNEdge_sid-E51B5FA9-FEEA-4FD5-AD16-653812CD5DB0">
        <omgdi:waypoint x="145.5" y="227.0"></omgdi:waypoint>
        <omgdi:waypoint x="190.5" y="227.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-F6EE8655-2911-41B1-A275-1EC0670FF7D5" id="BPMNEdge_sid-F6EE8655-2911-41B1-A275-1EC0670FF7D5">
        <omgdi:waypoint x="290.5" y="227.0"></omgdi:waypoint>
        <omgdi:waypoint x="335.5" y="227.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-90D7C165-4154-4329-AB3D-148B8575629C" id="BPMNEdge_sid-90D7C165-4154-4329-AB3D-148B8575629C">
        <omgdi:waypoint x="580.5" y="227.0"></omgdi:waypoint>
        <omgdi:waypoint x="625.5" y="227.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge bpmnElement="sid-5DFF304D-4C44-46B6-8FAD-9C235B0C6A74" id="BPMNEdge_sid-5DFF304D-4C44-46B6-8FAD-9C235B0C6A74">
        <omgdi:waypoint x="435.5" y="227.0"></omgdi:waypoint>
        <omgdi:waypoint x="480.5" y="227.0"></omgdi:waypoint>
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</definitions>
```
![](/imgs/process.png)
***
### 发布
![](/imgs/fabu.png)
### 部署
![](/imgs/bushu.png)
## 剩余问题
+ 时间关系，一些问题没有解决，有想法小伙伴可以留言IDEA
+ 中文乱码问题
+ 。。。。



